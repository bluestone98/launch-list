# Best practices
- Fix broken links
- W3C link checker
- Spelling and grammar
- Check website in all browsers
- Decide on www-subdomain

# Mobile
- MobileOK score of 75+
- Use 'viewport' meta-tag
- Use correct input types
- Manual check using emulators
- Test using real devices

# Analytics
- Uptime monitoring
- Traffic analysis

# Performance
- Google Page Speed score of 90+
- Yahoo YSlow score of 85+
- Optimize HTTP headers
- Optimize images

# Usability
- HTML5 compatibility check
- Custom 404 page
- Favicon
- Use SEO friendly URLs
- Print-friendly CSS
- Environment Integration

# Semantics
- Add meaning with Microdata
- Check the semantics

# SEO
- SenSEO score of 85+
- Google Rich Snippets
- robots.txt
- XML sitemap
- Code quality
- HTML validation
- CSS validation
- Run CSS Lint
- Run JSLint/JSHint
- World ready (W3C i18n checker)

# Accessibility
- Accessibility validation
- Color contrast
- WAI-ARIA Landmarks

# Security
- Follow best practices
- Cross-site scripting
- Cross-site request forgery
- Secure connection (SSL)
- HTTP Strict Transport Security
- Enable Content Security Policy

# Social Media
- Open Graph protocol
- Twitter Cards
- Facebook Insights
- Google+

# Extra
- humans.txt